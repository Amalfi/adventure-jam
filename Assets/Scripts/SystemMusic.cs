﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class SystemMusic : MonoBehaviour
{
    public AudioMixer mainMixer;
    public AudioClip[] clips;

    private AudioSource source;
    private int aux;

	// Use this for initialization
	private void Awake ()
    {
        source = GetComponent<AudioSource>();
        aux = 0;
        Play();
	}

    private void Play()
    {
        if (clips.Length > 0 && clips[aux] != null)
        {
            source.clip = clips[aux];
            source.Play();
        }
    }

    public void ChangeMusic()
    {
        aux = ExtendMathf.ChangeInCircleRange(++aux, clips.Length);
    }

    //Call this function and pass in the float parameter musicLvl to set the volume of the AudioMixerGroup Music in mainMixer
    public void SetMusicLevel(float musicLvl)
    {
        mainMixer.SetFloat("musicVol", musicLvl);
    }

    //Call this function and pass in the float parameter sfxLevel to set the volume of the AudioMixerGroup SoundFx in mainMixer
    public void SetSfxLevel(float sfxLevel)
    {
        mainMixer.SetFloat("sfxVol", sfxLevel);
    }

    void Update()
    {
        if (!PauseGame.pause && Input.GetKeyDown(KeyCode.Tab))
        {
            ChangeMusic();
            Play();
        }
    }


}
