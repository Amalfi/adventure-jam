﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    public static bool pause;
    public static PauseGame p;
    private static bool aux;

    private void Awake()
    {
        p = this;
    }

    public void Pause(bool value)
    {
        aux = value;
        if (pause)
        {
            Time.timeScale = 1;
        }
        Invoke("SetPause", 0.1f);
    }

    private void SetPause()
    {
        pause = aux;
        if (pause)
        {
            Time.timeScale = 0;
        }
    }
}
