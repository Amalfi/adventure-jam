﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PC : MonoBehaviour
{
    private Transform HUD;
    private Transform desktop;
    private Transform options;
    private Transform email;

    public enum State { Email, Desktop, Options}
    public State state;


    private void Awake()
    {
        HUD = GameObject.Find("HUD").transform;
        desktop = GameObject.Find("Desktop").transform;
        options = GameObject.Find("Options").transform;
        email = GameObject.Find("Email").transform;
        state = State.Desktop;
    }

    private void Start()
    {
        PauseGame.p.Pause(true);
    }

    public void ChangeMode(bool mode)
    {
        PauseGame.p.Pause(mode);

        transform.parent.GetComponent<Image>().enabled = mode;
        transform.gameObject.SetActive(mode);

        foreach (Transform tr in HUD)
        {
            tr.gameObject.SetActive(!mode);
        }
    }

    public void Exit()
    {
        //If we are running in a standalone build of the game
        #if UNITY_STANDALONE
        //Quit the application
        Application.Quit();
        #endif

        //If we are running in the editor
        #if UNITY_EDITOR
        //Stop playing the scene
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
    }

    public void Options(bool enter)
    {
        desktop.gameObject.SetActive(enter);
        foreach (Transform tr in options)
        {
            tr.gameObject.SetActive(!enter);
        }

        if (!enter)
        {
            state = State.Options;
        }
        else
        {
            state = State.Desktop;
        }
    }

    public void Emails(bool enter)
    {
        desktop.gameObject.SetActive(enter);

        foreach (Transform tr in email)
        {
            tr.gameObject.SetActive(!enter);
        }

        if (!enter)
        {
            state = State.Email;
        }
        else
        {
            state = State.Desktop;
        }
    }

    public void Update()
    {
        if (PauseGame.pause && Input.GetKeyDown(KeyCode.Escape))
        {
            if (state == State.Options)
            {
                Options(true);
            }
            else if (state == State.Email)
            {
                Emails(true);
            }
            else if (state == State.Desktop)
            {
                ChangeMode(false);
            }
        }
    }

}
