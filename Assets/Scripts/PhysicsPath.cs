﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PhysicsPath : MonoBehaviour
{
    public int mass;
    [SerializeField]
    public Vector3 dir;
    private DOTweenPath path;
    private Vector3 aux;

    private int iDBlock;

    public void Awake()
    {
        path = GetComponent<DOTweenPath>();
        aux = transform.position;
    }

    void OnCollisionEnter(Collision coll)
    {
        if (transform.IsChildOf(coll.transform) && mass >= coll.gameObject.GetComponent<Rigidbody>().mass)
        {
            Vector3 v = coll.transform.position;
            Vector3 p = transform.position;
            if ((dir.normalized.x > 0 && v.x > p.x) || (dir.normalized.x < 0 && v.x < p.x) || (dir.normalized.y > 0 && v.y > p.y) || (dir.normalized.y < 0 && v.y < p.y))
            {
                path.DOPause();
                iDBlock = coll.gameObject.GetInstanceID();
            }
        }
    }

    void OnCollisionExit(Collision coll)
    {
        if (coll.gameObject.GetInstanceID() == iDBlock)
        {
            path.DOPlay();
        }
    }

    void Update()
    {
        dir = transform.position - aux;
        aux = transform.position;
    }
}
