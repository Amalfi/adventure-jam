﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ink.Runtime;

public class Dialog : MonoBehaviour
{
    public int divide;
    public float timeMax;
    public static Dialog dialog;

    [SerializeField]
    private TextAsset inkJSONAsset;
    private Story story;
    private Text dialogueText;
    private Transform buttons;
    private List<string> divideText;
    private int aux;

    private bool isDialogue;
    private bool isChoose;

    // Use this for initialization
    void Awake ()
    {
        dialog = this;

        story = new Story(inkJSONAsset.text);
        dialogueText = transform.GetChild(0).GetComponent<Text>();
        buttons = transform.GetChild(1);
        divideText = new List<string>();
        isDialogue = false;
        isChoose = false;
        aux = 0;
        
    }

    void OnDestroy()
    {
        dialog = null;
    }

    void FinishTimeMax()
    {
        if (aux < divideText.Count - 1)
        {
            aux++;
            dialogueText.text = divideText[aux];
            Invoke("FinishTimeMax", timeMax);
        }
        else
        {
            ContinueDialog();
        }
    }

    void Update()
    {

        if (isChoose)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Choise(1);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Choise(2);
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Choise(3);
            }
        }
    }


    private void CreateDialogue(string path)
    {
        isDialogue = true;
        story.ChoosePathString(path);
        divideText = story.ContinueMaximally().Trim().Divide(divide);
        dialogueText.text = divideText[0];
        Invoke("FinishTimeMax", timeMax);
    }

    private void ContinueDialog()
    {
        dialogueText.text = null;
        aux = 0;

        if (story.currentChoices.Count > 0)
        {
            isChoose = true;
            for (int i = 0; i < story.currentChoices.Count; i++)
            {
                Choice choice = story.currentChoices[i];
                Transform button = buttons.GetChild(i);
                button.gameObject.SetActive(true);
                button.transform.GetChild(0).GetComponent<Text>().text = choice.text;
            }
        }
        else
        {
            foreach (Transform button in buttons)
            {
                button.gameObject.SetActive(false);
            }
            FinishDialog();
        }
    }

    void Choise(int i)
    {
        if (i <= story.currentChoices.Count)
        {
            isChoose = false;
            foreach (Transform button in buttons)
            {
                button.gameObject.SetActive(false);
            }
            story.ChooseChoiceIndex(i - 1);
            divideText = story.ContinueMaximally().Trim().Divide(divide);
            dialogueText.text = divideText[0];
            Invoke("FinishTimeMax", timeMax);
        }
    }

    private void FinishDialog()
    {
        isDialogue = false;
    }
}
