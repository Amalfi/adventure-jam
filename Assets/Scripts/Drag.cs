﻿using UnityEngine;

/// <summary>
/// Drag a rigidbody with the mouse using a spring joint.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class Drag : MonoBehaviour
{
    private static Transform playerTransform;

    public float limitRadius;
    public float force;
    public float damping;

    private float lenght;

    Transform jointTrans;
    float dragDepth;

    void Awake()
    {
        if (playerTransform == null)
        {
            playerTransform = GameObject.Find("PG").transform;
        }
        lenght = 0;
    }

    void OnMouseDown()
    {
        if (!PauseGame.pause && (playerTransform.position - transform.position).magnitude <= limitRadius)
        {
            HandleInputBegin(Input.mousePosition);
        }
    }

    void OnMouseUp()
    {
        if (jointTrans)
        {
            HandleInputEnd(Input.mousePosition);
        }
    }

    void OnMouseDrag()
    {
        if (jointTrans)
        {
            HandleInput(Input.mousePosition);
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.E))
        {
            lenght += Time.deltaTime * 2.5f;
        }
        if (Input.GetKey(KeyCode.R))
        {
            lenght -= Time.deltaTime * 2.5f;
        }
    }

    public void HandleInputBegin(Vector3 screenPosition)
    {
        var ray = Camera.main.ScreenPointToRay(screenPosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject.HasTag("Interactive"))
            {
                dragDepth = CameraPlane.CameraToPointDepth(Camera.main, hit.point);
                jointTrans = AttachJoint(hit.rigidbody, hit.point);
            }
        }
    }

    public void HandleInput(Vector3 screenPosition)
    {
        if (jointTrans == null)
        {
            return;
        }
        var worldPos = Camera.main.ScreenToWorldPoint(screenPosition);
        Vector3 aux = CameraPlane.ScreenToWorldPlanePoint(Camera.main, dragDepth, screenPosition);

        Vector3 aument = playerTransform.GetChild(0).forward * lenght;
        aux += aument;

        if ((playerTransform.position - aux).magnitude <= limitRadius)
        {
            jointTrans.position = aux;
        }
        else
        {
            HandleInputEnd(Input.mousePosition);
        }
    }

    public void HandleInputEnd(Vector3 screenPosition)
    {
        Destroy(jointTrans.gameObject);
        lenght = 0;
    }

    Transform AttachJoint(Rigidbody rb, Vector3 attachmentPosition)
    {
        GameObject go = new GameObject("Attachment Point");
        go.hideFlags = HideFlags.HideInHierarchy;
        go.transform.position = attachmentPosition;

        var newRb = go.AddComponent<Rigidbody>();
        newRb.isKinematic = true;

        var joint = go.AddComponent<ConfigurableJoint>();
        joint.connectedBody = rb;
        joint.configuredInWorldSpace = true;
        joint.xDrive = NewJointDrive(force, damping);
        joint.yDrive = NewJointDrive(force, damping);
        joint.zDrive = NewJointDrive(force, damping);
        joint.slerpDrive = NewJointDrive(force, damping);
        joint.rotationDriveMode = RotationDriveMode.Slerp;

        return go.transform;
    }

    private JointDrive NewJointDrive(float force, float damping)
    {
        JointDrive drive = new JointDrive();
        drive.positionSpring = force;
        drive.positionDamper = damping;
        drive.maximumForce = Mathf.Infinity;
        return drive;
    }
}