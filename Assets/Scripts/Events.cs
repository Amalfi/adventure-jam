﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Events : MonoBehaviour
{
    public static Events global;
    public List<string> stringEvents;
    public Dictionary<string, bool> events;

    // Use this for initialization
    void Awake()
    {
        global = this;

        events = new Dictionary<string, bool>();

        foreach (string e in stringEvents)
        {
            events.Add(e.ToLower(), false);
        }
    }

    public void OnDestroy()
    {
        global = null;
    }

    public bool CheckEvent(string s)
    {
        IsExist(s);
        return events[s.ToLower()];
    }


    public bool CheckEvent(string[] strings)
    {
        bool e = true;
        foreach (string s in strings)
        {
            e = CheckEvent(s);
            if (!e)
            {
                break;
            }
        }
        return e;
    }

    public bool CheckEvent(List<string> strings)
    {
        return CheckEvent(strings.ToArray());
    }

    public void ActiveEvent(string s, bool active)
    {
        IsExist(s);
        events[s.ToLower()] = active;
    }

    public void ActiveEvent(string[] strings, bool active)
    {
        foreach (string s in strings)
        {
            ActiveEvent(s, active);
        }
    }

    public void ActiveEvent(List<string> strings, bool active)
    {
        ActiveEvent(strings.ToArray(), active);
    }

    public void RestoreEvents()
    {
        events = events.ToDictionary(p => p.Key, p => false);
    }

    private void IsExist(string s)
    {
        if (!events.ContainsKey(s))
        {
            events.Add(s, false);
        }
    }
}
