﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour
{
    public int TimeSecondForMinute; 

    public static Clock clock;
    public List<TimeEvent> timeEvents;

    private int timeSlot;
    private ClockUI clockUI;

    private float auxTime;

    public delegate void ClickAction(int time);
    public static event ClickAction OnClicked;

    // Use this for initialization
    void Awake ()
    {
        clock = this;
        clockUI = GameObject.FindObjectOfType<ClockUI>();	
	}

    void Start()
    {
        AddTime(0);
    }

    public void OnDestroy()
    {
        clock = null;
    }

    public void AddTime(int time)
    {
        timeSlot += time;
        CheckTimeEvents();
        clockUI.AddTime(timeSlot);

        if (OnClicked != null)
        {
            OnClicked(timeSlot);
        }
    }

    void CheckTimeEvents()
    {
        List<TimeEvent> aux = new List<TimeEvent>(timeEvents);

        foreach (TimeEvent t in timeEvents)
        {
            if (timeSlot >= TransformTimeSlot(t.time) )
            {
                Events.global.events[t.ev] = t.value;
                aux.Remove(t);
            }
        }

        timeEvents = aux;
    }

    private int TransformTimeSlot(String time)
    {
        string[] arrayAux = time.Split(':');
        int our = 60 * Int32.Parse(arrayAux[0]);
        return Int32.Parse(arrayAux[1]) + our;
    }

    public void Update()
    {
        auxTime += Time.deltaTime;
        if (auxTime >= TimeSecondForMinute)
        {
            AddTime(1);
            auxTime = 0;
        }
    }

    [Serializable]
    public struct TimeEvent
    {
        public string time;
        public string ev;
        public bool value;
    }



}
