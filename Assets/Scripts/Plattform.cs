﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plattform : MonoBehaviour
{
    private Dictionary<int, Transform> listTransform;

    private void Awake()
    {
        listTransform = new Dictionary<int, Transform>();
        Vector3 aux = transform.localScale;
        aux = new Vector3(aux.x, aux.y / transform.parent.localScale.y, aux.z);
        transform.localScale = aux;
        transform.position = transform.parent.position + (Vector3.up * ( (transform.parent.GetComponent<BoxCollider>().bounds.size.y / 2) - 0.2f) );

    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.name != "PathObject")
        {
            listTransform.Add(coll.gameObject.GetInstanceID(), coll.transform);
            coll.transform.parent = transform;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        coll.transform.parent = null;
        listTransform.Remove(coll.gameObject.GetInstanceID());
    }
}
