﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtendMathf
{
    public static float ReturnAngleToDir(Vector2 dir)
    {
        return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
    }

    public static float ChangeRange(this float value, UnityEngine.Vector2 originalRange, UnityEngine.Vector2 newRange)
    {
        float scale = (float)(newRange.y - newRange.x) / (originalRange.y - originalRange.x);
        return (float)(newRange.x + ((value - originalRange.x) * scale));
    }

    public static int ChangeInCircleRange(int value, int max)
    {
        return ChangeInCirlceRange(value, new UnityEngine.Vector2(0, max));
    }

    public static int ChangeInCirlceRange(int value, UnityEngine.Vector2 range)
    {
        if (value >= range.y)
        {
            return (int)range.x;
        }
        return value;
    }
}
