﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtendString
{

    public static List<string> Divide(this string s, int divide)
    {
        string sub;
        List<string> list = new List<string>();
        if (s.Length >= divide)
        {
            int cont = 0;
            int last = 0;
            bool control = false;
            for (int i=0; i<s.Length; i++)
            {
                cont++;
                if ( (cont>=divide && (s[i] == ' ')) || (i==s.Length-1) )
                {
                    if (!control)
                    {
                        control = true;
                        sub = s.Substring(last, cont);
                    }
                    else
                    {
                        sub = s.Substring(last, cont);
                        if (sub.Length>1)
                        {
                            sub = sub.Substring(1,sub.Length-1);
                        }
                    }
                    list.Add(sub);
                    last = i;
                    cont = 0;
                }
            }
        }
        else
        {
            list.Add(s);
        }

        return list;
    } 

}
