﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlHUD : MonoBehaviour
{
    PC pc;

	// Use this for initialization
	void Awake ()
    {
        pc = transform.parent.FindChild("PC").GetComponent<PC>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!PauseGame.pause && Input.GetKeyDown(KeyCode.Escape))
        {
            pc.ChangeMode(true);
        }
    }
}
