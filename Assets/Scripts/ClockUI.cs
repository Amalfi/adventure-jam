﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClockUI : MonoBehaviour
{
    public int timeUnit;

    private Text textComponent;
    private int timeTotal;

    // Use this for initialization
    void Awake ()
    {
        textComponent = GetComponent<Text>();
        textComponent.text = "0:00";
        timeTotal = 0;
	}

    public void ClickTime(int time)
    {
        Clock.clock.AddTime(time);
    }


    public void AddTime(int time)
    {
        int our = (time / 60) % 24;
        string ourString = "";

        if (our < 10)
        {
            ourString = "0" + our;
        }
        else
        {
            ourString = ""+our;
        }

        int minute = time % 60;
        string minuteString = "";

        if (minute < 10)
        {
            minuteString = "0" + minute;
        }
        else
        {
            minuteString = "" + minute;
        }

        textComponent.text = ourString + ":" + minuteString;
    }
}
