﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeNPC : MonoBehaviour
{

    [SerializeField]
    private float m_halfConeSize = 45f;

    private LayerMask mask;

    private void Awake()
    {
        mask = ExtendLayer.AddLayer("Env");
    }

    //Step 1: With a sphere collider, we can figure out when things are
    // within a circle around us.

    void OnTriggerStay(Collider col)
    {
        if (col)
        {

        }

        Vector3 myPos = transform.position;
        Vector3 myVector = transform.forward;
        Vector3 theirPos = col.transform.position;
        Vector3 theirVector = theirPos - myPos;

        //Step 2: Is the object in front of this enemy?

        float mag = Vector3.SqrMagnitude(myVector) * Vector3.SqrMagnitude(theirVector);

        if (mag == 0f) //prevent divide by zero.  
            return;

        float dotProd = Vector3.Dot(myVector, theirPos - myPos);
        bool isNegative = dotProd < 0f;
        dotProd = dotProd * dotProd;
        //The Square operation will eliminate negative values, but we want to retain them.
        if (isNegative)
        {
            dotProd *= -1;
        }

        float sqrAngle = Mathf.Rad2Deg * Mathf.Acos(dotProd / mag);
        bool isInFront = sqrAngle < m_halfConeSize;
        if (col.gameObject.name == "Player")
        {
            print(sqrAngle + " " + col.gameObject.name);
        }

        //Step 3: Is there anything obscuring the object?

        Debug.DrawLine(myPos, theirPos, isInFront ? Color.green : Color.red);
        if (isInFront)
        {
            //Bit shift the layermask so that this linecast only hits actors we want it to.  
            // We want the Env objects to block our linecast.  
            if (!Physics.Linecast(myPos, theirPos, mask))
            {
                Debug.Log("SEE");
                //print("sensing something " + col.gameObject.name);  
                //SenseSomething(col.gameObject);
            }
        }
    }

}
