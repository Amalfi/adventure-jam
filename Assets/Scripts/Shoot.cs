﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public float velocity;
    public GameObject prefab;

	
	// Update is called once per frame
	void Update ()
    {
        if (!PauseGame.pause && Input.GetKeyDown(KeyCode.Q))
        {
            Vector3 dir = transform.GetChild(0).forward;
            GameObject obj = Instantiate(prefab, transform.position + dir, Quaternion.identity);
            obj.GetComponent<Rigidbody>().AddForce(dir * velocity);
        }

    }
}
