﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtendTag
{

    public static bool HasTags(this GameObject value, string[] tags)
    {
        if (tags != null & tags.Length != 0)
        {
            foreach (string tag in tags)
            {
                if (!value.HasTag(tag))
                {
                    return false;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool HasTags(this GameObject value, string tag1, string tag2)
    {
        return value.HasTags(new string[] { tag1, tag2 });
    }

    public static string[] ReturnTags(this GameObject value)
    {
        MultiTags multiTags = value.GetComponent<MultiTags>();
        if (multiTags)
        {
            List<MT> mts = multiTags.localTagList;
            string[] tags = new string[mts.Count];

            for (int i = 0; i < tags.Length; i++)
            {
                tags[i] = mts[i].Name;
            }
            return tags;
        }
        else
        {
            return new string[] { };
        }
    }

    public static string ReturnTag(this GameObject value)
    {
        string[] tags = value.ReturnTags();
        if (tags.Length > 0)
        {
            return tags[0];
        }
        else
        {
            return null;
        }
    }
}
